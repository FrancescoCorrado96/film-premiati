package esercitazioni.filmpremiati;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.SparseArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FilmDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "FilmDb.db";
    private static final int DATABASE_VERSION = 1;

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String NOT_NULL = " NOT NULL";
    private static final String COMMA_SEP = ",";

    private static final String FILM_TABLE_NAME = "film";

    private static final String ID = "id";
    private static final String TITLE = "titolo";
    private static final String YEAR = "anno";
    private static final String DIRECTOR = "regista";
    private static final String DIRECTOR_ID = "id_regista";
    private static final String AWARDS = "premi_vinti";

    private static final String AWARDS_TABLE_NAME = "premi";
    private static final String AWARD_TITLE = "nome";
    private static final String AWARD_TYPE = "tipo";
    private static final String AWARDS_ID_SEPARATOR = "/";

    private static final String DIRECTORS_TABLE_NAME = "registi";
    private static final String DIRECTOR_NAME = "nome";
    private static final String DIRECTOR_SURNAME = "cognome";
    private static final String DIRECTOR_NATIONALITY = "nazionalità";

    private static final String TABLE_FILM_CREATE = "CREATE TABLE " +
            FILM_TABLE_NAME + " (" +
            ID + " INTEGER PRIMARY KEY," +
            TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            YEAR + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            DIRECTOR + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            DIRECTOR_ID + INTEGER_TYPE + NOT_NULL + COMMA_SEP +
            AWARDS + TEXT_TYPE + NOT_NULL + ")";

    private static final String TABLE_AWARDS_CREATE = "CREATE TABLE " +
            AWARDS_TABLE_NAME + " (" +
            ID + " INTEGER PRIMARY KEY," +
            AWARD_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            AWARD_TYPE + TEXT_TYPE + NOT_NULL + ")";

    private static final String TABLE_DIRECTORS_CREATE = "CREATE TABLE " +
            DIRECTORS_TABLE_NAME + " (" +
            ID + " INTEGER PRIMARY KEY," +
            DIRECTOR_NAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            DIRECTOR_SURNAME + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            DIRECTOR_NATIONALITY + TEXT_TYPE + NOT_NULL + ")";

    FilmDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_FILM_CREATE);
        db.execSQL(TABLE_AWARDS_CREATE);
        db.execSQL(TABLE_DIRECTORS_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (newVersion) {
            case 2:
                break;
        }
    }

    int getNextFilmId() {
        return getNFilms() + 1;
    }

    int getNextAwardId() {
        return getNAwards() + 1;
    }

    int getNextDirectorId() {
        return  getNDirectors() + 1;
    }

    void addNewFilm(Film film) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues filmValues = new ContentValues();

        filmValues.put(ID, getNextFilmId());
        filmValues.put(TITLE, film.getTitle());
        filmValues.put(YEAR, film.getYear());
        filmValues.put(DIRECTOR, film.getDirector());
        filmValues.put(AWARDS, "");

        db.insert(FILM_TABLE_NAME, null, filmValues);
        db.close();
    }

    boolean addNewDirector(Director director) {
        if(isDirectorOnDb(director))
            return false;

        SQLiteDatabase db = getWritableDatabase();
        ContentValues val = new ContentValues();

        val.put(ID, getNextDirectorId());
        val.put(DIRECTOR_NAME, director.getName());
        val.put(DIRECTOR_SURNAME, director.getSurname());
        val.put(DIRECTOR_NATIONALITY, director.getNat());

        return db.insert(DIRECTORS_TABLE_NAME, null, val) > 0;
    }

    private boolean isDirectorOnDb(Director dir) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + DIRECTORS_TABLE_NAME + "" + " WHERE " +
                DIRECTOR_NAME + "=? AND " +
                DIRECTOR_SURNAME + "=? AND " +
                DIRECTOR_NATIONALITY + "=?", new String[] {dir.getName(), dir.getSurname(), dir.getNat()});
        boolean result = cursor.moveToFirst();
        cursor.close();
        return result;
    }

    void addAwardToFilm(Film film, Award award) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        String where = ID + "=" + Integer.toString(film.getId());

        insertAwardOnDb(award);

        String oldAwards = getOldAwards(where);

        if (oldAwards.isEmpty())
            values.put(AWARDS, Integer.toString(award.getId()));
        else
            values.put(AWARDS, oldAwards + AWARDS_ID_SEPARATOR + Integer.toString(award.getId()));

        db.update(FILM_TABLE_NAME, values, where, null);
        db.close();
    }

    Film selectFilm(int id) {
        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.query(FILM_TABLE_NAME, null, ID + "=?", new String[]{Integer.toString(id)}, null, null, null);
        if (!cursor.moveToFirst())
            return null;

        String title = cursor.getString(cursor.getColumnIndex(TITLE));
        int year = cursor.getInt(cursor.getColumnIndex(YEAR));
        int director = cursor.getInt(cursor.getColumnIndex(DIRECTOR_ID));

        cursor.close();

        return new Film(id, title, year, director);
    }

    void deleteFilm(Film film) {
        SQLiteDatabase db = getWritableDatabase();

        db.delete(FILM_TABLE_NAME, ID + "=?", new String[]{Integer.toString(film.getId())});

        db.close();
    }

    List<Film> getFilmList() {
        SQLiteDatabase db = getReadableDatabase();
        List<Film> films = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM " + FILM_TABLE_NAME, null);

        if (cursor == null || !cursor.moveToFirst())
            return films;

        for (int i = 0; i < cursor.getCount(); i++) {

            String title = cursor.getString(cursor.getColumnIndex(TITLE));
            int year = cursor.getInt(cursor.getColumnIndex(YEAR));
            int director = cursor.getInt(cursor.getColumnIndex(DIRECTOR_ID));

            int id = cursor.getInt(cursor.getColumnIndex(ID));

            Film film = new Film(id, title, year, director);

            films.add(film);
            cursor.moveToNext();
        }
        db.close();
        cursor.close();
        return films;
    }

    List<Director> getDirectorList() {
        SQLiteDatabase db = getReadableDatabase();
        List<Director> directors = new ArrayList<>();
        SparseArray<String> dirSparseArray = new SparseArray<>();

        Cursor cursor = db.rawQuery("SELECT * FROM " + FILM_TABLE_NAME, null);
        if (!cursor.moveToFirst())
            return directors;

        for (int i = 0; i < cursor.getCount(); i++) {
            String awardsIds = cursor.getString(cursor.getColumnIndex(AWARDS));
            Integer directorId = cursor.getInt(cursor.getColumnIndex(DIRECTOR_ID));

            if(dirSparseArray.indexOfKey(directorId) < 0) {
                dirSparseArray.append(directorId, awardsIds);
            } else {
                String oldAwards = dirSparseArray.get(directorId);
                String newAwards = oldAwards + AWARDS_ID_SEPARATOR + awardsIds;
                dirSparseArray.put(directorId, newAwards);
            }
            cursor.moveToNext();
        }
        cursor.close();

        for(int i = 0; i < dirSparseArray.size(); i++) {
            int id = dirSparseArray.keyAt(i);
            directors.add(new Director(id, dirSparseArray.get(id)));
        }

        return directors;
    }

    List<Award> getListAwardsFromAwardType(Film film, int type) {
        List<Award> allAwards = getListAwardsFromFilmId(film.getId());
        List<Award> awardsFromType = new ArrayList<>();

        if (allAwards == null)
            return awardsFromType;

        for (Award a : allAwards) {
            if (a == null)
                return awardsFromType;
            if (a.getType() == type) {
                awardsFromType.add(a);
            }
        }
        return awardsFromType;
    }

    private int getNFilms() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + FILM_TABLE_NAME, null);

        int nFilms = cursor.getCount();
        cursor.close();

        return nFilms;
    }

    private int getNAwards() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + AWARDS_TABLE_NAME, null);

        int nAwards = cursor.getCount();
        cursor.close();

        return nAwards;
    }

    private int getNDirectors() {
        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + DIRECTORS_TABLE_NAME, null);
        int nDirectors = cursor.getCount();
        cursor.close();

        return nDirectors;
    }

    private void insertAwardOnDb(Award award) {
        SQLiteDatabase db = getReadableDatabase();
        ContentValues cv = new ContentValues();

        cv.put(AWARD_TITLE, award.getTitle());
        cv.put(AWARD_TYPE, award.getType());
        int nAwards = getNAwards() + 1;
        cv.put(ID, nAwards);

        db.insert(AWARDS_TABLE_NAME, null, cv);
    }

    private List<Award> getListAwardsFromFilmId(int id) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(FILM_TABLE_NAME, null, ID + "=?", new String[]{Integer.toString(id)}, null, null, null);
        if (!cursor.moveToFirst())
            return null;

        String awardsString = cursor.getString(cursor.getColumnIndex(AWARDS));
        List<String> awardIds = new ArrayList<>();

        if (awardsString.contains(AWARDS_ID_SEPARATOR))
            awardIds = Arrays.asList(awardsString.split(AWARDS_ID_SEPARATOR));
        else
            awardIds.add(awardsString);

        cursor.close();

        List<Award> awards = new ArrayList<>();

        for (String a : awardIds) {
            if (!a.isEmpty())
                awards.add(getAwardFromId(db, Integer.parseInt(a)));
        }

        return awards;
    }

    private Award getAwardFromId(SQLiteDatabase db, int id) {
        Cursor cursor = db.query(AWARDS_TABLE_NAME, null, ID + "=?", new String[]{Integer.toString(id)}, null, null, null);
        if (!cursor.moveToFirst())
            return null;
        int type = cursor.getInt(cursor.getColumnIndex(AWARD_TYPE));
        String title = cursor.getString(cursor.getColumnIndex(AWARD_TITLE));
        cursor.close();
        return new Award(id, type, title);
    }

    private String getOldAwards(String where) {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(FILM_TABLE_NAME, null, where, null, null, null, null);
        if (!cursor.moveToFirst())
            return "";
        String oldAwards = cursor.getString(cursor.getColumnIndex(AWARDS));
        if(oldAwards == null)
            return "";
        cursor.close();
        return oldAwards;
    }
}
