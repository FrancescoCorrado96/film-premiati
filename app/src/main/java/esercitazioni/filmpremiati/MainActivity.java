package esercitazioni.filmpremiati;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;

public class MainActivity extends AppCompatActivity {
    protected void openNewActivity(Class cls) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
    }
}
