package esercitazioni.filmpremiati;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddFilmActivity extends MainActivity{

    final FilmDbHelper db = new FilmDbHelper(AddFilmActivity.this);

    @BindView(R.id.insert_title)
    EditText etInsertTitle;

    @BindView(R.id.insert_year)
    EditText etInsertYear;

    @BindView(R.id.insert_director)
    EditText etInsertDirector;

    @BindView(R.id.ab_toolbar_list_film)
    Toolbar tbMain;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_film);
        ButterKnife.bind(this);

        setSupportActionBar(tbMain);
        getSupportActionBar().setTitle(R.string.add_new_film);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @OnClick(R.id.submit)
    public void onClick() {
        String title = etInsertTitle.getText().toString();
        String yearString = etInsertYear.getText().toString();
        String director = etInsertDirector.getText().toString();

        int id = db.getNextFilmId();

        if(!title.isEmpty() && !yearString.isEmpty() && !director.isEmpty()) {
            int year = Integer.parseInt(yearString);
            Film newFilm = new Film(id, title, year, director);
            db.addNewFilm(newFilm);
            Toast.makeText(AddFilmActivity.this, R.string.added_successfully, Toast.LENGTH_SHORT).show();
            onBackPressed();
        } else {
            Toast.makeText(AddFilmActivity.this, R.string.something_is_missing, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
