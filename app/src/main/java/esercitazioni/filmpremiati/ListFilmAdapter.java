package esercitazioni.filmpremiati;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListFilmAdapter extends RecyclerView.Adapter<ListFilmAdapter.FilmViewHolder> {

    private List<Film> listFilms;
    private final Context cx;
    private final FilmDbHelper db;

    ListFilmAdapter(Context context, FilmDbHelper db) {
        this.db = db;
        this.listFilms = new ArrayList<>(db.getFilmList());
        this.cx = context;
    }

    @NonNull
    @Override
    public FilmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_film_layout, parent, false);
        return new FilmViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull FilmViewHolder holder, int position) {
        final Film actualFilm = listFilms.get(listFilms.size() - position - 1);

        holder.txvFilmTitle.setText(actualFilm.getTitle());
        holder.txvYear.setText(Integer.toString(actualFilm.getYear()));
        holder.txvDirector.setText(actualFilm.getDirector());

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(cx, FilmDetailActivity.class);
                intent.putExtra("film", actualFilm.getId());
                cx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(listFilms.isEmpty())
            return 0;
        return listFilms.size();
    }

    void refresh() {
        this.listFilms = db.getFilmList();
    }

    public class FilmViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        private final TextView txvFilmTitle;
        private final TextView txvYear;
        private final TextView txvDirector;


        private FilmViewHolder(View v) {
            super(v);
            view = v;
            txvFilmTitle = view.findViewById(R.id.txv_title_on_list);
            txvYear = view.findViewById(R.id.txv_year_on_list);
            txvDirector = view.findViewById(R.id.txv_director_on_list);
        }
    }
}
