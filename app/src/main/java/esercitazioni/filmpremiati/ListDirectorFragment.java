package esercitazioni.filmpremiati;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListDirectorFragment extends Fragment {

    FilmDbHelper db;

    @BindView(R.id.rcv_list_director)
    RecyclerView rcvListDirector;

    ListDirectorAdapter mAdapter;

    public ListDirectorFragment() {}

    public static ListDirectorFragment newInstance() {
        return new ListDirectorFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_director_list, container, false);
        ButterKnife.bind(this, view);
        db = new FilmDbHelper(getActivity());

        rcvListDirector.setHasFixedSize(true);

        mAdapter = new ListDirectorAdapter(getActivity(), db);

        rcvListDirector.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvListDirector.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.refresh();
        rcvListDirector.getAdapter().notifyDataSetChanged();
        rcvListDirector.invalidate();
    }
}
