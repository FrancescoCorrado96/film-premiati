package esercitazioni.filmpremiati;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilmDetailActivity extends MainActivity {

    Film actualFilm;
    FilmDbHelper db;
    int filmId;
    int actualAwardType;
    ListAwardsAdapter oscarAdapter;
    ListAwardsAdapter goldenGlobeAdapter;

    EditText etDialogTitle;

    @BindView(R.id.txv_title)
    TextView txvTitle;

    @BindView(R.id.txv_director)
    TextView txvDirector;

    @BindView(R.id.txv_year)
    TextView txvYear;

    @BindView(R.id.rcv_oscars)
    RecyclerView rcvOscars;

    @BindView(R.id.rcv_golden_globes)
    RecyclerView rcvGoldenGlobes;

    @BindView(R.id.ab_toolbar_list_film)
    Toolbar tbMain;

    @BindView(R.id.fab_add_award)
    FloatingActionButton fabAddAward;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_detail);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        filmId = intent.getExtras().getInt("film");

        db = new FilmDbHelper(FilmDetailActivity.this);

        actualFilm = db.selectFilm(filmId);

        setSupportActionBar(tbMain);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        txvTitle.setText(actualFilm.getTitle());
        txvDirector.setText(actualFilm.getDirectorId());
        txvYear.setText(Integer.toString(actualFilm.getYear()));

        oscarAdapter = new ListAwardsAdapter(db, actualFilm, AwardEnum.OSCAR.toInt());
        goldenGlobeAdapter = new ListAwardsAdapter(db, actualFilm, AwardEnum.GOLDEN_GLOBE.toInt());

        rcvOscars.setHasFixedSize(true);
        rcvOscars.setLayoutManager(new LinearLayoutManager(FilmDetailActivity.this));
        rcvOscars.setAdapter(oscarAdapter);
        rcvOscars.setNestedScrollingEnabled(false);

        rcvGoldenGlobes.setHasFixedSize(true);
        rcvGoldenGlobes.setLayoutManager(new LinearLayoutManager(FilmDetailActivity.this));
        rcvGoldenGlobes.setAdapter(goldenGlobeAdapter);
        rcvGoldenGlobes.setNestedScrollingEnabled(false);
    }

    @OnClick(R.id.btn_delete)
    public void onDeleteClick() {
        new AlertDialog.Builder(FilmDetailActivity.this)
                .setTitle(R.string.delete_film)
                .setMessage(getString(R.string.are_you_sure) + actualFilm.getTitle() + " dal database?")
                .setPositiveButton(R.string.affermative_response, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        db.deleteFilm(actualFilm);
                        Toast.makeText(FilmDetailActivity.this, R.string.film_deleted, Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    }
                })
                .setNegativeButton(R.string.negative_response, null)
                .show();

    }

    @OnClick(R.id.fab_add_award)
    public void onAddAwardClick() {

        View dialogView = getLayoutInflater().inflate(R.layout.dialog_layout, null);

        etDialogTitle = dialogView.findViewById(R.id.et_dialog_title);

        AlertDialog.Builder dialog = new AlertDialog.Builder(FilmDetailActivity.this)
                .setTitle(R.string.add_award)
                .setView(dialogView)
                .setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Award newAward = new Award(db.getNextAwardId(), actualAwardType, etDialogTitle.getText().toString());
                        db.addAwardToFilm(actualFilm, newAward);
                        if (actualAwardType == AwardEnum.OSCAR.toInt()) {
                            oscarAdapter.refresh();
                            rcvOscars.getAdapter().notifyDataSetChanged();
                            rcvOscars.invalidate();
                            Toast.makeText(FilmDetailActivity.this, "Premio aggiunto!", Toast.LENGTH_SHORT).show();
                        } else {
                            goldenGlobeAdapter.refresh();
                            rcvGoldenGlobes.getAdapter().notifyDataSetChanged();
                            rcvGoldenGlobes.invalidate();
                        }
                    }
                })
                .setNegativeButton(R.string.discard, null);

        Spinner spinner = dialogView.findViewById(R.id.spn_award_type);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.awards_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                actualAwardType = i;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        dialog.show();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
