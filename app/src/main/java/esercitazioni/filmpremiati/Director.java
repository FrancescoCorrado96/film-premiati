package esercitazioni.filmpremiati;

public class Director {
    private int id;
    private String awards;
    private String name;
    private String surname;
    private String nat;

    Director(int id, String awards) {
        this.id = id;
        this.awards = awards;
    }

    Director(int id, String name, String surname, String nationality) {
        this.id = id;
        this.awards = "";
        this.nat = nationality;
        this.name = name;
        this.surname = surname;
    }

    public int getId() {
        return id;
    }

    public String getAwards() {
        return awards;
    }

    public void setAwards(String awards) {
        this.awards = awards;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getNat() {
        return nat;
    }
}
