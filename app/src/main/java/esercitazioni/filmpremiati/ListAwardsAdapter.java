package esercitazioni.filmpremiati;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListAwardsAdapter extends RecyclerView.Adapter<ListAwardsAdapter.AwardViewHolder> {

    private List<Award> awards;
    private FilmDbHelper db;
    private int type;
    private Film film;

    ListAwardsAdapter(FilmDbHelper db, Film film, int type) {
        this.film = film;
        this.type = type;
        this.db = db;
        this.awards = new ArrayList<>(db.getListAwardsFromAwardType(film, type));
    }

    @NonNull
    @Override
    public AwardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_award_layout, parent, false);
        return new AwardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AwardViewHolder holder, int position) {
        final Award actualAward = awards.get(position);

        holder.txvAward.setText(actualAward.getTitle());
    }

    @Override
    public int getItemCount() {
        if(awards.isEmpty())
            return 0;
        return awards.size();
    }

    void refresh() {
        this.awards = db.getListAwardsFromAwardType(film, type);
    }

    public class AwardViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        private final TextView txvAward;

        private AwardViewHolder(View v) {
            super(v);
            view = v;
            txvAward = view.findViewById(R.id.txv_award);
        }
    }
}
