package esercitazioni.filmpremiati;

public class Film {

    private int id;
    private String title;
    private int year;
    private int directorId;
    private String director;

    Film(int id, String title, int year, int directorId) {
        this.id = id;
        this.directorId = directorId;
        this.title = title;
        this.year = year;
    }

    Film(int id, String title, int year, String director) {
        this.id = id;
        this.directorId = 0;
        this.director = director;
        this.title = title;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return this.year;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDirectorId() {
        return directorId;
    }

    public String getDirector() {
        return director;
    }

}
