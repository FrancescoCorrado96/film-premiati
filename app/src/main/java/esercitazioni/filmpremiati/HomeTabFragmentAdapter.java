package esercitazioni.filmpremiati;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class HomeTabFragmentAdapter extends FragmentPagerAdapter {
    private static final int NUM_PAGES = 2;

    protected HomeTabFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new ListFilmFragment();
        } else if (position == 1){
            return new ListDirectorFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Film";
            case 1:
                return "Registi";
            default:
                return null;
        }
    }
}
