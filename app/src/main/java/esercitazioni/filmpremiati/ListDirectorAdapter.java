package esercitazioni.filmpremiati;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ListDirectorAdapter extends RecyclerView.Adapter<ListDirectorAdapter.DirectorViewHolder> {

    private List<Director> listDirectors;
    private final Context cx;
    private final FilmDbHelper db;

    ListDirectorAdapter(Context context, FilmDbHelper db) {
        this.db = db;
        this.listDirectors = new ArrayList<>(db.getDirectorList());
        this.cx = context;
    }

    @NonNull
    @Override
    public DirectorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.element_director_layout, parent, false);
        return new DirectorViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull DirectorViewHolder holder, int position) {
        final Director actualDirector = listDirectors.get(position);

        holder.txvDirectorName.setText(actualDirector.getName());
        //holder.txvNumberAwards.setText(Integer.toString(actualDirector.getFilmDirected().size()));

        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(cx, DirectorDetailActivity.class);
                cx.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if(listDirectors.isEmpty())
            return 0;
        return listDirectors.size();
    }

    void refresh() {
        this.listDirectors = db.getDirectorList();
    }

    public class DirectorViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        private final TextView txvDirectorName;
        private final TextView txvNumberAwards;


        private DirectorViewHolder(View v) {
            super(v);
            view = v;
            txvDirectorName = view.findViewById(R.id.txv_director_on_director_list);
            txvNumberAwards = view.findViewById(R.id.txv_awards_on_list);
        }
    }
}
