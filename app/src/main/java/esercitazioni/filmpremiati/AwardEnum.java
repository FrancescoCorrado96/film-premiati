package esercitazioni.filmpremiati;

public enum AwardEnum {
    OSCAR (0),
    GOLDEN_GLOBE(1);

    int type;

    AwardEnum(int type) {
        this.type = type;
    }

    public int toInt() {
        return type;
    }
}
