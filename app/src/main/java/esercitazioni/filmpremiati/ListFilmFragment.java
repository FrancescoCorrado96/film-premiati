package esercitazioni.filmpremiati;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.jetbrains.annotations.Nullable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListFilmFragment extends Fragment {

    FilmDbHelper db;

    @BindView(R.id.rcv_list_film)
    RecyclerView rcvFilmList;

    ListFilmAdapter mAdapter;

    public ListFilmFragment() {}

    public static ListFilmFragment newInstance() {
        return new ListFilmFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_film_list, container, false);
        ButterKnife.bind(this, view);
        db = new FilmDbHelper(getActivity());

        rcvFilmList.setHasFixedSize(true);

        mAdapter = new ListFilmAdapter(getActivity(), db);

        rcvFilmList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvFilmList.setAdapter(mAdapter);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.refresh();
        rcvFilmList.getAdapter().notifyDataSetChanged();
        rcvFilmList.invalidate();
    }
}
