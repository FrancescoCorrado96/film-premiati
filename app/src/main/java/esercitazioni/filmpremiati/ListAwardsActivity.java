package esercitazioni.filmpremiati;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListAwardsActivity extends MainActivity {

    @BindView(R.id.llt_awards_container)
    LinearLayout lltAwardsContainer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_awards);
        ButterKnife.bind(this);
        //todo Lista dinamica di elementi Expandable Layout
        for(int i = 0; i < 5; i++) {
            TextView txvAward = new TextView(ListAwardsActivity.this);
            txvAward.setText(String.valueOf(i));
            lltAwardsContainer.addView(txvAward, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        }
    }
}
