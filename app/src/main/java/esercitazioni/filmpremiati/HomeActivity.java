package esercitazioni.filmpremiati;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends MainActivity {

    @BindView(R.id.vp_home)
    ViewPager vpHome;

    @BindView(R.id.tab_list_film)
    TabLayout tabListFilm;

    @BindView(R.id.ab_toolbar_list_film)
    Toolbar abToolbarListFilm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film_list);
        ButterKnife.bind(this);

        setSupportActionBar(abToolbarListFilm);
        getSupportActionBar().setTitle("Home premi");

        vpHome.setAdapter(new HomeTabFragmentAdapter(getSupportFragmentManager()));
        tabListFilm.setupWithViewPager(vpHome);

    }

    @OnClick(R.id.fab_add)
    public void onFilmClick(){
        openNewActivity(AddFilmActivity.class);
    }
}
